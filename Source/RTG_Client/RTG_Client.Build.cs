// Fill out your copyright notice in the Description page of Project Settings.

using UnrealBuildTool;
using System.IO;

public class RTG_Client : ModuleRules
{
	public RTG_Client(TargetInfo Target)
	{
		PublicDependencyModuleNames.AddRange(new string[] { "Core", "CoreUObject", "Engine", "InputCore", "RakNet" });

		PrivateDependencyModuleNames.AddRange(new string[] {  });

        // Uncomment if you are using Slate UI
        // PrivateDependencyModuleNames.AddRange(new string[] { "Slate", "SlateCore" });

        // Uncomment if you are using online features
        // PrivateDependencyModuleNames.Add("OnlineSubsystem");

        // To include OnlineSubsystemSteam, add it to the plugins section in your uproject file with the Enabled attribute set to true

        LoadRTG(Target);
	}

    private string ThirdPartyPath
    {
        get { return Path.GetFullPath(Path.Combine(ModuleDirectory, "../../ThirdParty/")); }
    }

    public bool LoadRTG(TargetInfo Target)
    {
        bool bSupported = false;
        if ((Target.Platform == UnrealTargetPlatform.Win64))
        {
            bSupported = true;

            string PlatformString = (Target.Platform == UnrealTargetPlatform.Win64) ? "x64" : "x86";
            string LibrariesPath = Path.Combine(ThirdPartyPath, "rtg_shared", "Libraries");

            // Force release mode for now since UE4 doesn't use Debug CRT
            string libraryNameSuffix = (Target.Configuration == UnrealTargetConfiguration.DebugGame) ||
               (Target.Configuration == UnrealTargetConfiguration.Debug) ? "" : ""; 

            //string FinalLibPath = Target
            PublicAdditionalLibraries.Add(Path.Combine(LibrariesPath, "RTG_Shared" + libraryNameSuffix + ".lib"));
            //PublicAdditionalLibraries.Add(Path.Combine(LibrariesPath, "Jaylen." + PlatformString + ".lib")); 
        }

        if (bSupported)
        {
            // Include path
            PublicIncludePaths.Add(Path.Combine(ThirdPartyPath, "RTG_Shared", "Includes"));
        }

        Definitions.Add(string.Format("WITH_RTG_MAIN_BINDING={0}", bSupported ? 1 : 0));

        return bSupported;
    }
}
