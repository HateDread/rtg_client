
#pragma once

#include "rtg_shared/Game/Entity/SpaceID.h"
#include "DelegateCombinations.h"

namespace SystemEvents
{
	namespace Types
	{
		DECLARE_MULTICAST_DELEGATE_OneParam(FOnNewProxyReceived, SpaceID);
		DECLARE_MULTICAST_DELEGATE_OneParam(FOnNewProxiesReceived, const TArray<SpaceID>&);
	}

	extern Types::FOnNewProxyReceived OnNewProxyReceived;
	extern Types::FOnNewProxiesReceived OnNewProxiesReceived;
}
