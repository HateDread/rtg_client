#pragma once

class UClientSystem;
class UClass;
class UObject;
class NetworkPublisher;

namespace ClientSystemUtils
{
UClientSystem* Create(UClass* Class, NetworkPublisher* a_networkPublisher, UObject* Outer = (UObject*)GetTransientPackage());
};
