#pragma once

#include "Object.h"
#include <Tickable.h>
//#include "NetworkUpdater.generated.h"

class NetworkSystem;

/**
* Temporary way to move updating the NetworkSystem out of the GameMode and into some separate class.
* May be eventually moved towards the beginning of UGameEngine::Tick or similar.
*/
//UCLASS()
class NetworkUpdater : public FTickableGameObject
{
public:

	//GENERATED_BODY()

	NetworkUpdater(NetworkSystem* a_networkSystem);

	virtual void Tick(float DeltaTime) override;
	virtual bool IsTickable() const override;
	virtual TStatId GetStatId() const override;

	virtual bool IsTickableWhenPaused() const override;
	virtual bool IsTickableInEditor() const override;

private:
	NetworkSystem* m_networkSystem;

};
