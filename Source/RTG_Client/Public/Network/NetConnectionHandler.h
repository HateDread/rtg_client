#pragma once

#include "rtg_shared/Network/INetworkSubscriber.h"

class UClientSystem;
class UClientVisualizer;

class NetConnectionHandler : public INetworkSubscriber
{
public:
	NetConnectionHandler(UClientSystem* a_clientSystem, UClientVisualizer* a_clientVisualizer);

	virtual void Subscribe(NetworkPublisher* a_networkPublisher) override;
	virtual void Unsubscribe(NetworkPublisher* a_networkPublisher) override;

private:
	void OnInitialSync(const RakNet::Packet* a_packet);

	UClientSystem* m_clientSystem;
	UClientVisualizer* m_clientVisualizer;

};
