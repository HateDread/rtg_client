#pragma once

#include <memory>

class NetworkPublisher;

namespace RakNet
{
	class RakPeerInterface;
	struct AddressOrGUID;
}

class NetworkSystem
{
public:
	NetworkSystem();
	~NetworkSystem();

	void Update();
	void ConnectToServer(const RakNet::AddressOrGUID& a_gameAddr);

	NetworkPublisher* GetNetworkPublisher();

private:
	// temporary location for the binding of net callbacks
	//void BindNetworkCallbacks();

	RakNet::RakPeerInterface* m_raknet;
	std::unique_ptr<NetworkPublisher> m_networkPublisher;
};