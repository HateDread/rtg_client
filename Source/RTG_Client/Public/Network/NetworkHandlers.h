#pragma once

#include "UniquePtr.h"

class UClientSystem;
class NetConnectionHandler;
class NetworkPublisher;
class UClientVisualizer;

class NetworkHandlers
{
public:
	NetworkHandlers(UClientSystem* a_clientSystem, UClientVisualizer* a_clientVisualizer);
	~NetworkHandlers();

	void Subscribe(NetworkPublisher* a_networkPublisher);

protected:
	UClientSystem* m_clientSystem;

private:
	void CreateHandlers(UClientVisualizer* a_clientVisualizer);

	TUniquePtr<NetConnectionHandler> m_netConnectionHandler;

};
