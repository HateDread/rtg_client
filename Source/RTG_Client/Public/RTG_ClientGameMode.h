// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "GameFramework/GameMode.h"
#include "NetworkSystem.h"
#include "NetworkUpdater.h"
#include "RTG_ClientGameMode.generated.h"

class UClientSystem;

/**
 * 
 */
UCLASS()
class RTG_CLIENT_API ARTG_ClientGameMode : public AGameMode
{
	GENERATED_BODY()

	ARTG_ClientGameMode();

	void BeginPlay() override;
	void StartMatch() override;
	void Tick(float DeltaSeconds) override;
	void EndPlay(EEndPlayReason::Type EndPlayReason) override;

protected:
	TUniquePtr<NetworkSystem> m_networkSystem;
	TUniquePtr<NetworkUpdater> m_networkUpdater;

	UPROPERTY(EditDefaultsOnly, Category = "RTG|Core")
	TSubclassOf<UClientSystem> ClientSystemClass;

	UPROPERTY(BlueprintReadOnly, Category = "RTG|Core")
	UClientSystem* m_clientSystem;

};
