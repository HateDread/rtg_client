
#pragma once

#include "Game/ProxyActor.h"

class AProxyActor;
class UWorld;
class UClass;

namespace ProxyActorUtils
{
AProxyActor* Create(UWorld* a_world, UClass* a_proxyClass = AProxyActor::StaticClass(), const FTransform& a_transform = FTransform(),
	const FActorSpawnParameters& a_params = FActorSpawnParameters());
}
