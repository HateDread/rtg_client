#pragma once

#include "GameFramework/Actor.h"
#include "ProxyActor.generated.h"


UCLASS()
class AProxyActor : public AActor
{
public:
	GENERATED_BODY()

	AProxyActor();

protected:

	UPROPERTY(BlueprintReadOnly, EditDefaultsOnly, Category = "Components")
	UBoxComponent* m_debugBox;

	UPROPERTY(BlueprintReadOnly, EditDefaultsOnly, Category = "Components")
	UBillboardComponent* m_billboard;

};
