
#pragma once

#include "Object.h"
#include "Game/ProxyActors.h"
#include "ClientVisualizer.generated.h"

class AProxyActor;

UCLASS()
class UClientVisualizer : public UObject
{
public:
	GENERATED_BODY()

	UClientVisualizer();
	~UClientVisualizer();

	inline void SetWorld(UWorld* a_world) {
		m_world = a_world;
	};

	inline UWorld* GetWorld() const override {
		return m_world;
	}

	ProxyActors& GetProxyActors();

private:
	UWorld* m_world;
	ProxyActors m_proxyActors;

};
