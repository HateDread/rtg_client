
#pragma once

#include "Map.h"
#include "rtg_shared/Game/Entity/SpaceID.h"

class AProxyActor;

class ProxyActors
{
public:
	// change to creating proxy internally?
	void RegisterProxy(SpaceID a_ID, AProxyActor* a_proxy);
	AProxyActor* GetProxy(SpaceID a_ID);

private:
	TArray<AProxyActor*> m_actors;
	TMap<SpaceID, size_t> m_IDToIndex;

};
