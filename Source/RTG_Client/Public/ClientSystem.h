#pragma once

#include "rtg_shared/Network/INetworkSubscriber.h"
#include "NetworkHandlers.h"
#include "ClientSystem.generated.h"

class UClientVisualizer;
class UWorld;

/**
* Updating may be moved to UGameEngine::Tick or similar depending on threading choices
*/
UCLASS(Blueprintable)
class UClientSystem : public UObject
{
private:
	GENERATED_BODY()

	UClientSystem();

public:

	void Initialize(TUniquePtr<NetworkHandlers>&& a_networkHandlers, UClientVisualizer* a_clientVisualizer, UWorld* a_world);

	void Update(float a_deltaTime);


protected:
	UPROPERTY(BlueprintReadWrite, EditDefaultsOnly, Category = "RTG|Core")
	float ScaleDivider = 1000000000.0f;

private:
	TUniquePtr<NetworkHandlers> m_networkHandlers;

	UPROPERTY()
	UClientVisualizer* m_clientVisualizer;

	// client visualizer
	// client state / data
	// object holding ID:AActor-subclass mappings
	// ptr to AHUD?

};