#include "RTG_Client.h"
#include "NetworkSystem.h"
#include "MessageIdentifiers.h"
#include "rtg_shared/Network/NetworkPublisher.h"
#include "RakPeerInterface.h"
#include "RakNetTypes.h"
#include "rtg_shared/Network/PacketTypes.h"
#include "BitStream.h"

NetworkSystem::NetworkSystem()
	: m_networkPublisher(std::make_unique<NetworkPublisher>())
{
	m_raknet = RakNet::RakPeerInterface::GetInstance();

	RakNet::SocketDescriptor desc;
	m_raknet->Startup(1, &desc, 1);
}

NetworkSystem::~NetworkSystem()
{
	m_raknet->Shutdown(100);
	RakNet::RakPeerInterface::DestroyInstance(m_raknet);
}

void NetworkSystem::Update()
{
	std::vector<RakNet::Packet*> packets;

	RakNet::Packet* packet = m_raknet->Receive();
	while (packet != nullptr)
	{
		packets.push_back(packet);
		packet = m_raknet->Receive();
	}

	m_networkPublisher->Update(packets);

	for (int i = 0; i < packets.size(); ++i)
	{
		m_raknet->DeallocatePacket(packets[i]);
	}
}

void NetworkSystem::ConnectToServer(const RakNet::AddressOrGUID& a_gameAddr)
{
	char servIp[25];
	a_gameAddr.ToString(false, servIp);
	RakNet::ConnectionAttemptResult result = m_raknet->Connect(servIp, a_gameAddr.systemAddress.GetPort(), nullptr, 0);

	ensure(result == RakNet::CONNECTION_ATTEMPT_STARTED);
}

NetworkPublisher* NetworkSystem::GetNetworkPublisher()
{
	return m_networkPublisher.get();
}

/*void NetworkSystem::BindNetworkCallbacks()
{
	m_networkPublisher->Register(ID_CONNECTION_REQUEST_ACCEPTED, [](auto a_packet) {
		GEngine->AddOnScreenDebugMessage(-1, 10, FColor::Cyan, "Connection request accepted.");
	});

	m_networkPublisher->Register(ID_CONNECTION_ATTEMPT_FAILED, [](auto a_packet) {
		GEngine->AddOnScreenDebugMessage(-1, 10, FColor::Cyan, "Connection attempt failed.");
	});

	m_networkPublisher->Register(ID_CONNECTION_LOST, [](auto a_packet) {
		GEngine->AddOnScreenDebugMessage(-1, 10, FColor::Cyan, "Connection lost.");
	});

	m_networkPublisher->Register(ID_DISCONNECTION_NOTIFICATION, [](auto a_packet) {
		GEngine->AddOnScreenDebugMessage(-1, 10, FColor::Cyan, "Disconnection received.");
	});

	m_networkPublisher->Register(EPacketType::SERVER_INITIAL_SYNC, [this](auto a_packet) {
		GEngine->AddOnScreenDebugMessage(-1, 10, FColor::Cyan, "Sync received.");

		RakNet::BitStream input(a_packet->data, a_packet->length, true);

		input.IgnoreBytes(1);

		int playerID;
		input.Read(playerID);
		int numOtherObjs;
		input.Read(numOtherObjs);

		RakNet::BitStream bs;
		bs.Write((unsigned char)EPacketType::CLIENT_SYNC_ACK);
		m_raknet->Send(&bs, PacketPriority::HIGH_PRIORITY, RELIABLE_ORDERED, 0, a_packet->systemAddress, false);
	});
}*/
