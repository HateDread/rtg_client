#include "RTG_Client.h"
#include "NetworkHandlers.h"
#include "NetConnectionHandler.h"
#include "rtg_shared/Network/INetworkSubscriber.h"
#include "Game/ClientVisualizer.h"


NetworkHandlers::NetworkHandlers(UClientSystem* a_clientSystem, UClientVisualizer* a_clientVisualizer)
	: m_clientSystem(a_clientSystem)
{
	CreateHandlers(a_clientVisualizer);
}

NetworkHandlers::~NetworkHandlers() = default;

void NetworkHandlers::Subscribe(NetworkPublisher* a_networkPublisher)
{
	m_netConnectionHandler->Subscribe(a_networkPublisher);
}

void NetworkHandlers::CreateHandlers(UClientVisualizer* a_clientVisualizer)
{
	m_netConnectionHandler = TUniquePtr<NetConnectionHandler>(new NetConnectionHandler(m_clientSystem, a_clientVisualizer));
}
