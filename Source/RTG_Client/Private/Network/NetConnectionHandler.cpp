#include "RTG_Client.h"
#include "NetConnectionHandler.h"

#include "MessageIdentifiers.h"
#include "rtg_shared/Network/PacketTypes.h"
#include "rtg_shared/Network/NetworkPublisher.h"
#include "BitStream.h"

#include "ClientSystem.h"
#include "Game/ClientVisualizer.h"
#include "Utility/SystemEvents.h"


NetConnectionHandler::NetConnectionHandler(UClientSystem* a_clientSystem, UClientVisualizer* a_clientVisualizer)
	: m_clientSystem(a_clientSystem),
	m_clientVisualizer(a_clientVisualizer)
{

}

void NetConnectionHandler::Subscribe(NetworkPublisher* a_networkPublisher)
{
	a_networkPublisher->Register(ID_CONNECTION_REQUEST_ACCEPTED, [](auto a_packet) {
		GEngine->AddOnScreenDebugMessage(-1, 10, FColor::Cyan, "Connection request accepted.");
	});

	a_networkPublisher->Register(ID_CONNECTION_ATTEMPT_FAILED, [](auto a_packet) {
		GEngine->AddOnScreenDebugMessage(-1, 10, FColor::Cyan, "Connection attempt failed.");
	});

	a_networkPublisher->Register(ID_CONNECTION_LOST, [](auto a_packet) {
		GEngine->AddOnScreenDebugMessage(-1, 10, FColor::Cyan, "Connection lost.");
	});

	a_networkPublisher->Register(ID_DISCONNECTION_NOTIFICATION, [](auto a_packet) {
		GEngine->AddOnScreenDebugMessage(-1, 10, FColor::Cyan, "Disconnection received.");
	});

	a_networkPublisher->Register(EPacketType::SERVER_INITIAL_SYNC, PacketCallback(&NetConnectionHandler::OnInitialSync, this));
}

void NetConnectionHandler::Unsubscribe(NetworkPublisher* a_networkPublisher)
{
	a_networkPublisher->UnregisterAll(this);
}

void NetConnectionHandler::OnInitialSync(const RakNet::Packet* a_packet)
{
	GEngine->AddOnScreenDebugMessage(-1, 10, FColor::Cyan, "Sync received.");

	RakNet::BitStream input(a_packet->data, a_packet->length, true);

	input.IgnoreBytes(1);

	SpaceID playerID;
	input.Read(playerID);
	int numObjs;
	input.Read(numObjs);

	TArray<SpaceID> IDs;
	IDs.SetNum(numObjs);

	input.Read((char*)IDs.GetData(), sizeof(SpaceID) * numObjs);

	SystemEvents::OnNewProxiesReceived.Broadcast(IDs);
}
