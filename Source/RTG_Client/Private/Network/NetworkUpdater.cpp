#include "RTG_Client.h"
#include "NetworkUpdater.h"
#include "NetworkSystem.h"

NetworkUpdater::NetworkUpdater(NetworkSystem* a_networkSystem)
	: m_networkSystem(a_networkSystem)
{

}

void NetworkUpdater::Tick(float DeltaTime)
{
	m_networkSystem->Update();
}

bool NetworkUpdater::IsTickable() const
{
	return true;
}

TStatId NetworkUpdater::GetStatId() const
{
	return TStatId();
}

bool NetworkUpdater::IsTickableWhenPaused() const
{
	return true;
}

bool NetworkUpdater::IsTickableInEditor() const
{
	return true;
}
