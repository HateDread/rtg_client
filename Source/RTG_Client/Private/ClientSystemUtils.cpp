#include "RTG_Client.h"
#include "ClientSystemUtils.h"
#include "ClientSystem.h"
#include "NetworkHandlers.h"
#include "Game/ClientVisualizer.h"

namespace ClientSystemUtils
{
UClientSystem* Create(UClass* Class, NetworkPublisher* a_networkPublisher, UObject* Outer /*= (UObject*)GetTransientPackage()*/)
{
	UClientSystem* clientSystem = NewObject<UClientSystem>(Outer, Class, FName("ClientSystem"));

	UClientVisualizer* visualizer = NewObject<UClientVisualizer>(Outer, FName("ClientVisualizer"));
	TUniquePtr<NetworkHandlers> netHandlers = TUniquePtr<NetworkHandlers>(new NetworkHandlers(clientSystem, visualizer));
	netHandlers->Subscribe(a_networkPublisher);

	ensure(Outer->GetWorld() != nullptr);

	clientSystem->Initialize(std::move(netHandlers), visualizer, Outer->GetWorld());

	return clientSystem;
}
}
