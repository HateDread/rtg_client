// Fill out your copyright notice in the Description page of Project Settings.

#include "RTG_Client.h"
#include "RTG_ClientGameMode.h"

#include "MessageIdentifiers.h"
#include "BitStream.h"

#include "rtg_shared/Network/PacketTypes.h"

#include "ClientSystem.h"
#include "ClientSystemUtils.h"

ARTG_ClientGameMode::ARTG_ClientGameMode()
	: Super()
{
	bDelayedStart = true;
}

void ARTG_ClientGameMode::BeginPlay()
{
	Super::BeginPlay();

	m_networkSystem = TUniquePtr<NetworkSystem>(new NetworkSystem());
	m_networkUpdater = TUniquePtr<NetworkUpdater>(new NetworkUpdater(m_networkSystem.Get()));

	checkf(ClientSystemClass != nullptr, TEXT("GameMode's ClientSystemClass not set!"));

	m_clientSystem = ClientSystemUtils::Create(ClientSystemClass, m_networkSystem->GetNetworkPublisher(), this);

	m_networkSystem->ConnectToServer(RakNet::SystemAddress("127.0.0.1", 12001));
}

void ARTG_ClientGameMode::StartMatch()
{
	Super::StartMatch();
}

void ARTG_ClientGameMode::Tick(float DeltaSeconds)
{
	Super::Tick(DeltaSeconds);

	// early-out
	if (!HasMatchStarted())
		return;

	m_clientSystem->Update(DeltaSeconds);

}

void ARTG_ClientGameMode::EndPlay(EEndPlayReason::Type EndPlayReason)
{
	Super::EndPlay(EndPlayReason);

	//m_networkSystem.Release();
	//m_networkUpdater.Release();

	//m_clientSystem->ConditionalBeginDestroy();
}
