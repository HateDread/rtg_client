#include "RTG_Client.h"
#include "ProxyActors.h"

void ProxyActors::RegisterProxy(SpaceID a_ID, AProxyActor* a_proxy)
{
	m_actors.Add(a_proxy);
	m_IDToIndex.Add(a_ID, m_actors.Num() - 1);
}

AProxyActor* ProxyActors::GetProxy(SpaceID a_ID)
{
	auto actorIdx = m_IDToIndex.Find(a_ID);
	if (actorIdx != nullptr)
	{
		return m_actors[*actorIdx];
	}
	else
	{
		return nullptr;
	}
}
