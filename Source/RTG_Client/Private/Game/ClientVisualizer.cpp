#include "RTG_Client.h"
#include "Game/ClientVisualizer.h"
#include "Utility/SystemEvents.h"
#include "Game/ProxyActorUtils.h"

void ProxyReceived(SpaceID a_ID, ProxyActors& a_proxyActors, UWorld* a_world)
{
	AProxyActor* proxy = ProxyActorUtils::Create(a_world);
	a_proxyActors.RegisterProxy(a_ID, proxy);
}

UClientVisualizer::UClientVisualizer()
{
	if (!IsDefaultSubobject())
	{
		SystemEvents::OnNewProxyReceived.AddLambda([this](SpaceID a_ID) {
			ProxyReceived(a_ID, m_proxyActors, m_world);
		});

		SystemEvents::OnNewProxiesReceived.AddLambda([this](const TArray<SpaceID>& a_IDs) {
			for (const SpaceID& ID : a_IDs)
			{
				ProxyReceived(ID, m_proxyActors, m_world);
			}
		});
	}
}

UClientVisualizer::~UClientVisualizer()
{
	// have to clear these because they're static and will
	// stick around between PIE runs. Need a better solution!
	SystemEvents::OnNewProxiesReceived.Clear();
	SystemEvents::OnNewProxyReceived.Clear();
}

ProxyActors& UClientVisualizer::GetProxyActors()
{
	return m_proxyActors;
}
