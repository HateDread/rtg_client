#include "RTG_Client.h"
#include "Game/ProxyActorUtils.h"
#include "Game/ProxyActor.h"
#include "Engine/World.h"

namespace ProxyActorUtils
{
AProxyActor* Create(UWorld* a_world, UClass* a_proxyClass /*= AProxyActor::StaticClass()*/,
	const FTransform& a_transform /*= FTransform()*/, const FActorSpawnParameters& a_params /*= FActorSpawnParameters()*/)
{
	return a_world->SpawnActor<AProxyActor>(AProxyActor::StaticClass(), a_transform, a_params);
}
}
