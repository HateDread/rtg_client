#include "RTG_Client.h"
#include "Game/ProxyActor.h"

AProxyActor::AProxyActor()
	: Super()
{
	m_debugBox = CreateDefaultSubobject<UBoxComponent>(FName("BoxComponent"));
	RootComponent = m_debugBox;

	m_debugBox->bHiddenInGame = false;

	m_billboard = CreateDefaultSubobject<UBillboardComponent>(FName("Billboard"));
	m_billboard->bHiddenInGame = false;
	m_billboard->SetupAttachment(m_debugBox);// m_debugBox, FAttachmentTransformRules(EAttachmentRule::KeepRelative,
		//EAttachmentRule::KeepRelative, EAttachmentRule::KeepRelative, false));
}
