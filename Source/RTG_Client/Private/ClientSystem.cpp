#include "RTG_Client.h"
#include "ClientSystem.h"
#include "NetworkHandlers.h"
#include "Game/ClientVisualizer.h"

UClientSystem::UClientSystem()
{

}

void UClientSystem::Initialize(TUniquePtr<NetworkHandlers>&& a_networkHandlers, UClientVisualizer* a_clientVisualizer, UWorld* a_world)
{
	m_networkHandlers = std::move(a_networkHandlers);
	m_clientVisualizer = a_clientVisualizer;
	m_clientVisualizer->SetWorld(a_world);
}

void UClientSystem::Update(float a_deltaTime)
{

}
